import link from 'next/link';

const Footer = () => {
    return (
        <>
          {/* Main Footer */}
          <footer className="main-footer">
            {/* To the right */}
            <div className="float-right d-none d-sm-inline">
              {/* เว็บไซต์นี้จัดทำขึ้นเพื่อการศึกษาเท่านั้น: แหล่งข่าวจาก มติชน  */}
              <strong>News source from Matichon</strong>
            </div>
            {/* Default to the left */}
            This website is for educational purposes only
          </footer>
        </>
    )
}

export default Footer;