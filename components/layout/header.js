import link from 'next/link';

const Header = () => {
    return (
        <>
          {/* Navbar */}
          <nav className="main-header navbar navbar-expand-md navbar-light navbar-white">
          <div className="container">
              <a href="/" className="navbar-brand">
              <img src="dist/img/Logo.jpg" alt="AdminLTE Logo" className="brand-image img-circle elevation-3" style={{opacity: '.8'}} />
              <span className="brand-text font-weight-light">หยุดเชื้อเพื่อชาติ</span>
              </a>
              <button className="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon" />
              </button>
              <div className="collapse navbar-collapse order-3" id="navbarCollapse">
              {/* Left navbar links */}
              <ul className="navbar-nav">
                  <li className="nav-item">
                  <a href="/" className="nav-link">หน้าแรก</a>
                  </li>
                  <li className="nav-item">
                  <a href="https://www.facebook.com/หยุดเชื้อเพื่อชาติ-108588508101726" className="nav-link">Fans PageFacebook</a>
                  </li>
              </ul>
              </div>
          </div>
          </nav>
          {/* /.navbar */}
        </>
    )
}

export default Header;