const express = require('express')
const request = require('request')
const cheerio = require("cheerio")
const cors = require('cors')

const app = express()
const port = 4000

app.use(cors())
app.get('/news/:page', (req, res) => {
  var news = [];
  var page = req.params.page;
  const url = "https://www.matichon.co.th/covid19/thai-covid19" + (page == 1 ? "" : `/page/${page}`)
  request(url, function (error, response, body) {
    if (error) {
      res.send(response.statusCode);
    }
    
    var $ = cheerio.load(body);
    if(page == 1){
      $('div.td_module_2').each(function (index, element) {
        news[index] = {};
        const url = $(element).find('a.ud-module-link').attr('href').split("/");
        news[index]['news_id'] = url[url.length-2]+"/"+url[url.length-1];
        news[index]['photo'] = $(element).find('img.entry-thumb').attr('src');
        news[index]['title'] = $(element).find('h3.entry-title').text().trim();
        news[index]['time'] = $(element).find('time.entry-date').text().trim();
        news[index]['detail'] = $(element).find('div.td-excerpt').text().trim();
      });
    }
    
    $('div.td_module_10').each(function (index, element) {
      let newIndex = (page == 1 ? index + 2 : index)
      news[newIndex] = {};
      const url = $(element).find('a.ud-module-link').attr('href').split("/");
      news[newIndex]['news_id'] = url[url.length-2] + "/" + url[url.length-1];
      news[newIndex]['photo'] = $(element).find('img.entry-thumb').attr('src');
      news[newIndex]['title'] = $(element).find('h3.entry-title').text().trim();
      news[newIndex]['time'] = $(element).find('time.entry-date').text().trim();
      news[newIndex]['detail'] = $(element).find('div.td-excerpt').text().trim();
    });
    
    res.json(news);
  })
})

app.get('/detail/:type/:news_id', (req, res) => {
  const url = "https://www.matichon.co.th/" + req.params.type + "/" + req.params.news_id
  request(url, function (error, response, body) {
    if (error) {
      res.send(response.statusCode);
    }
    
    var $ = cheerio.load(body);
    $('div.td-ss-main-content').each(function (index, element) {
      newDetail = {};
      newDetail['photo'] = $(element).find('img.entry-thumb').attr('src');
      newDetail['title'] = $(element).find('h1.entry-title').text().trim();
      newDetail['time'] = $(element).find('time.entry-date').text().trim();

      var detail = $(element).find('p').map(function() {
        const data = $(this).text().trim().replace("\n", "");
        if(!data == "") return data;
      }).toArray();
      newDetail['detail'] = detail.join(`\n`);
    });
    res.json(newDetail);
  })
})

app.get('/totalpage', (req, res) => {
  const url = "https://www.matichon.co.th/covid19/thai-covid19"
  request(url, function (error, response, body) {
    if (error) {
      res.send(response.statusCode);
    }
    
    var $ = cheerio.load(body);
    $('div.page-nav').each(function (index, element) {
      totalPage = {}
      totalPage['totalPage'] = $(element).find('a.last').text();
    });
    res.json(totalPage);
  })
})
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})