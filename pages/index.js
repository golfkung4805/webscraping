import Head from 'next/head';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Layout from '../components/layout/main';

export default function Home() {
  const [news, setNews] = useState({});
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(1050);
  const [newsList, setNewsList] = useState([{}]);

  const [loading, setLoading] = useState(false);
  const [readLoading, setReadLoading] = useState(false);

  useEffect(()=>{
    const fetchData = async () => {
      setLoading(true);
      const newsResult = await axios.get('http://localhost:4000/news/' + page)
      // const totalP = await axios.get('http://localhost:4000/totalpage')
      // setTotalPage(totalP.data.totalPage)
      setNewsList(newsResult.data)
      setLoading(false);
    } 
    fetchData();
  },[page])

  const readNews = async (news_id) => {
    setNews({})
    setReadLoading(true)
    $('#modal-news').modal('show')
    await axios.get('http://localhost:4000/detail/' + news_id)
    .then(function (response) {
      setNews({
        photo: response.data.photo,
        title: response.data.title,
        time: response.data.time,
        detail: (response.data.detail)
      })
    })
    .catch(function (error) {
      console.log(error);
    });
    await setReadLoading(false)
  }

  const closeModal = () => {
    setNews({})
    $('#modal-news').modal('hide')
  }
  
  return (
    <Layout>
      <Head>
        <title>หยุดเชื้อเพื่อชาติ : News</title>
      </Head>
      {/* Content Wrapper. Contains page content */}
      <div className="content-wrapper">
        {/* Main content */}
        <div className="content">
          <div className="container pt-5 pb-5">
            <div className="card t-5">
              <div className="card-header pb-0">
                <div className="content-header p-0">
                  <div className="container">
                    <div className="row mb-2">
                      <div className="col-sm-6">
                        <h1 className="m-0">โควิด-ไทย</h1>
                      </div>{/* /.col */}
                      <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                          <li className="breadcrumb-item active">หน้าแรก</li>
                          <li className="breadcrumb-item active">เกาะติด Covid-19</li>
                          <li className="breadcrumb-item active">โควิด-ไทย</li>
                          <li className="breadcrumb-item active">#{page.toLocaleString('en')}</li>
                        </ol>
                      </div>{/* /.col */}
                    </div>{/* /.row */}
                  </div>{/* /.container-fluid */}
                </div>
                {/* /.content-header */}
              </div>
              { (loading) ? 
                <div className="card-body">
                  <div className="overlay"><i className="fas fa-5x fa-spinner fa-pulse"></i></div>
                </div> : 
                <div className="card-body">
                  { (newsList.length > 10)? 
                    <div className="row">
                      { newsList.map((data, index)=>{
                        if(index < 2){
                          return (
                            <div className="col-lg-6" key={`news_${index}`}>
                              <div className="card card-primary card-outline">
                                <div className="ribbon-wrapper ribbon-lg">
                                  <div className="ribbon bg-danger text-lg">ข่าวล่าสุด</div>
                                </div>
                                <img className="card-img-top" src={data.photo} alt={data.title} />
                                <div className="card-body">
                                  <h5 className="card-title">{data.title}</h5>
                                  <p className="card-text ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{data.detail}</p>
                                  <p className="card-text text-right">{data.time}</p>
                                </div>
                                <div className="card-footer text-muted">
                                  <button type="button" className="btn btn-primary" onClick={()=> readNews(data.news_id)}>อ่านเพิ่มเติม</button>
                                </div>
                              </div>
                            </div>
                            )
                        }
                      })}
                      
                      {/* /.col-md-6 */}
                    </div>
                    : "" 
                  }{/* /.row */}

                  <div className="row">
                    { newsList.map((data, index)=>{ 
                      const sIndex = (newsList.length > 10)? 2 : 0;
                      if(index >= sIndex){
                        return (
                          <div className="col-lg-4" key={`news_${index}`}>
                            <div className="card card-primary card-outline">
                              <img className="card-img-top" src={data.photo} alt={data.title} />
                              <div className="card-body">
                                <h5 className="card-title">{data.title}</h5>
                                <p className="card-text ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{data.detail}</p>
                                <p className="card-text text-right">{data.time}</p>
                              </div>
                              <div className="card-footer text-muted">
                                <button type="button" className="btn btn-primary" onClick={()=> readNews(data.news_id)}>อ่านเพิ่มเติม</button>
                              </div>
                            </div>
                        </div>
                        );
                      }
                    }) }
                  </div>{/* /.row */}
                </div>
              }
              <div className="card-footer text-muted">
                <nav aria-label="navigation">
                  <ul class="pagination justify-content-center">
                    <li class={`page-item ${page==1?'disabled' :''}`}><a class="page-link" onClick={()=> setPage(page-1)}>&laquo;</a></li>
                    <li class={`page-item ${page<3?'d-none':''} ${page==1?'active':''}`}><a class="page-link" onClick={()=> setPage(1)}>1</a></li>
                    <li class={`page-item disabled ${page<3?'d-none':''}`}><a class="page-link" href="#">...</a></li>
                    <li class={`page-item ${page<=1?'d-none':''} ${page==page-1?'active':''}`}><a class="page-link" onClick={()=> setPage(page-1)}>{(page-1).toLocaleString('en')}</a></li>
                    <li class={`page-item ${page==page?'active':''}`}><a class="page-link" onClick={()=> setPage(page)}>{page.toLocaleString('en')}</a></li>
                    <li class={`page-item ${page>=totalPage?'d-none':''} ${page==page+1?'active':''}`}><a class="page-link" onClick={()=> setPage(page+1)}>{(page+1).toLocaleString('en')}</a></li>
                    <li class={`page-item disabled ${page>=totalPage-1?'d-none':''}`}><a class="page-link" href="#">...</a></li>
                    <li class={`page-item ${page>=totalPage-1?'d-none':''} ${page == totalPage ? 'active' : ''}`}><a class="page-link" onClick={()=> setPage(totalPage)}>{totalPage.toLocaleString('en')}</a></li>
                    <li class={`page-item ${page==totalPage?'disabled' :''}`}><a class="page-link" onClick={()=> setPage(page+1)}>&raquo;</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>{/* /.container-fluid */}
        </div>
        {/* /.content */}
      </div>
      {/* /.content-wrapper */}
      <div className="modal fade" id="modal-news">
        <div className="modal-dialog modal-xl">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">{news['title']}</h4>
              <button type="button" className="close" onClick={closeModal}>
                <span aria-hidden="true">×</span>
              </button>
            </div>
            { (readLoading) ? 
              <div className="modal-body" style={{height: 532}} >
                <div className="overlay"><i className="fas fa-5x fa-spinner fa-pulse"></i></div>
              </div> : 
              <div className="modal-body">
                <div className="row">
                  <div className="col-lg-12 ">
                    <img src={news['photo']} alt={news['title']} className="img-thumbnail d-block mx-auto" />
                  </div>
                  <div className="col-lg-12 pt-3">
                    <p style={{whiteSpace: 'pre-line'}}>{news['detail']}</p>
                    <p className="text-right">{news['time']}</p>
                  </div>
                </div>
              </div>
            }
            <div className="modal-footer justify-content-end">
              <button type="button" className="btn btn-default" onClick={closeModal}>ปิดข่าวนี้</button>
            </div>
          </div>
          {/* /.modal-content */}
        </div>
        {/* /.modal-dialog */}
      </div>
      {/* /.modal */}

    </Layout>
  )
}
