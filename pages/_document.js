import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <Html lang="th">
        <Head>
          <meta charSet="utf-8" />
          <meta content="text/html; charset=UTF-8" name="Content-Type" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1"/>

          {/* <!-- Google Font: Source Sans Pro --> */}
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"/>
          {/* <!-- Font Awesome Icons --> */}
          <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css"/>
          {/* <!-- Theme style --> */}
          <link rel="stylesheet" href="/dist/css/adminlte.min.css" />
        </Head>
        <body className="hold-transition layout-top-nav">
        <div class="wrapper">
          <div id="fb-root"></div>
            <Main />
            <NextScript />
        </div>

        {/* <!-- jQuery --> */}
        <script src="/plugins/jquery/jquery.min.js"></script>
        {/* <!-- Bootstrap 4 --> */}
        <script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        {/* <!-- AdminLTE App --> */}
        <script src="/dist/js/adminlte.min.js"></script>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
